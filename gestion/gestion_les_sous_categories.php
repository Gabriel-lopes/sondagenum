<p class="flow-text">Les Sous Catégories</p>
<br>
<?php

//On récupère les catégories (mères)
$unControleur->setTable("categorie"); 
$lesCategories = $unControleur->selectAll();

//affichage de la table sondage (pour récupérer le nombre de sondage avec une boucle)
$unControleur->setTable("sondage");
$lesSondages = $unControleur->selectAll();

//Affichage des sous-catégories
$unControleur->setTable("sous_categorie"); 
$lesSousCategories = $unControleur->selectAll();
require_once ("vue/vue_les_sous_categories.php");

?>