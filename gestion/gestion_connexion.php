<?php 

$_SESSION['session'] = session_id();

if(isset($_POST['SeConnecter']))
{

    $mdphash = hash('sha512', $_POST['mdp']);

    $where = array("email"=>$_POST['email'],
                   "mdp"=>$mdphash);
    $unControleur->setTable("sondeur");
    $unUser = $unControleur->selectWhere($where);

    if (isset($unUser['email']))
    {
        $_SESSION['email']= $unUser['email'];
        $_SESSION['role']= $unUser['role'];
        $_SESSION['nom']= $unUser['nom'];
        $_SESSION['id']= $unUser['idSondeur'];
        header("Location: index.php");
    }else{
            echo '<script language="javascript">';
            echo 'alert("Veuillez verifier vos identifiants")';
            echo '</script>';
    }
}

require_once ("vue/vue_connexion.php");

?>