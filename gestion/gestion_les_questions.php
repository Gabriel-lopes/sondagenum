<?php

$id = $_GET['question_sondage'];

?>

<p class="flow-text">Sondages</p>
<br>
<br>
	
<?php 
if(isset($_SESSION['id'])) {
?>
<!-- Bouton pour créer un sondage -->
<a href="index.php?insert_question=<?= $id; ?>" class="waves-effect waves-light btn-large"><i class="material-icons left">cloud</i>Créer une question</a>
<br>
<br>
<?php 
}
?>

<?php

//On récupère les sondages
$unControleur->setTable("vue_sondage"); 
$lesVuesSondages = $unControleur->selectAll();

//On récupère les réponses des sondages
$unControleur->setTable("vue_reponse");
$lesVuesReponses = $unControleur->selectAll();

require_once("vue/vue_les_questions.php");
?>


