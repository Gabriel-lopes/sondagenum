<?php

//INSCRIPTION 



if(isset($_POST['inscription_entreprise']))
    {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $mdphash = hash('sha512', $_POST['mdp']);
        $tab = array("nom"=> trim($_POST['nom']), 
                    "email"=> trim($_POST['email']),
                    "tel"=>trim($_POST['tel']), 
                    "adresse"=>trim($_POST['adresse']),
                    "mdp"=>$mdphash,
                    "role"=>trim('user'),
                    "libelle"=>trim($_POST['libelle']), 
                    "sigle"=>trim($_POST['sigle']),
                    "siret"=>trim($_POST['siret']));

        if(!$unControleur->findUserByEmail($tab['email'])){
            $unControleur->insertProcedure("insertEntreprise", $tab);
            header("Location: index.php?connexion");
        }
        echo '<script language="javascript">';
        echo 'alert("Ce compte est déja pris, veuillez en choisir un autre")';
        echo '</script>';
               
    }
    
require_once("vue/vue_insert_les_entreprises.php");

?>