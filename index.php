<?php 
session_start();
require_once("controleur/config_bdd.php");
require_once("controleur/controleur.class.php");

// instanciation de la classe Controleur
$unControleur = new Controleur($server, $bdd, $user, $mdp);
?>

<!DOCTYPE html>
<html>
    <head>     
        <meta charset="utf-8">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
    <nav>
    <div class="container">
    <div class="nav-wrapper">
      <a href="index.php?" class="brand-logo"><i class="material-icons">cloud</i>Sondage-Num</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <?php
            if(!isset($_SESSION['id'])) {
            ?>
                <li><a href="index.php?connexion">Se connecter</a></li>
                <li><a href="index.php?inscription">S'inscrire</a></li>
            <?php 
            } else {
            ?>

                <li><font color="white"><?= "Bienvenue ".$_SESSION['nom']; ?></font></li>
                <li><a href="index.php?deconnexion">Se Déconnecter</a></li>
            <?php 
            }
            ?>
        </ul>
    </div>
    </div>
    </nav>
    <div class="container" align="center">
    <?php
    //Affichage des éléments
    if(isset($_GET['idCategorie'])) { //si on a un lien avec un id de categorie (si on a cliqué sur une catégorie)
        require_once("gestion/gestion_les_sous_categories.php"); //On affiche la page ou il y a toutes ses sous-catégories
    
    } else if(isset($_GET['idSous_categorie'])) { //si on a un lien avec un id de sous-catégorie (si on a cliqué sur une sous-catégorie)
        require_once("gestion/gestion_les_sondages.php"); //On affiche la page ou il y a tout ses sondages
    
    } else if(isset($_GET['resultat_sondage'])) {
        require_once("gestion/gestion_les_resultats.php");
    
    } else if(isset($_GET['vote_sondage']) /* AND si connecté AND si sondeur = personne (uniquement) */ ) {
        require_once("gestion/gestion_les_votes.php");
    
    } else if((isset($_GET['question_sondage']))){
        require_once("gestion/gestion_les_questions.php");
        
    } else if(isset($_GET['insert_question'])) {
        require_once("gestion/gestion_insert_questions.php");
    
    } else if(isset($_GET['connexion'])) {
        require_once("gestion/gestion_connexion.php");
    
    } else if(isset($_GET['inscription'])) {
        require_once("gestion/gestion_inscription.php");

    } else if(isset($_GET['Particulier'])) {
        require_once("gestion/gestion_insert_les_personnes.php");

    } else if(isset($_GET['Entreprise'])) {
        require_once("gestion/gestion_insert_les_entreprises.php");

    }else if(isset($_GET['deconnexion'])) {
        unset($_SESSION); 
        session_destroy();
        header("Location:index.php");
    
    } else {
        require_once("gestion/gestion_les_categories.php");
    }
    ?> 
    </div>
    <script type="text/javascript" src="js/materialize.js"></script>
    <footer class="page-footer">
            <div class="row">
              <div class="col l6 s6">
                <h5 class="white-text">INSEE</h5>
                <p class="grey-text text-lighten-4">Institut national de la statistique et des études économiques</p>
              </div>
              <div class="col l4 offset-l2 s6">
                <h5 class="white-text">Lien</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="//www.insee.fr/"><b>Insee</b></a></li>                    
                </ul>
              </div>
            </div>
            <b>©2022 Copyright</b>
    </footer>
    </body>
</html>