<?php 

if(!isset($_SESSION['id'])) { //Si non connecté
  header('Location: index.php'); //Redirection dans la page principale
}

?>

<div>
    <h5 class="center-align">Créer votre question</h5>
</div>
<br><br><br><br>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.css" />
</head>
<body> 
<div class = "container">
<div class="row">
  <?php
if ( ! isset($nbReponses) || $nbReponses <=1 ){
  echo '
    <form class="col s12" action="#" method="POST">
    <input type="text" name="question" placeholder="Question">
    <br><br> 
    
    <input type="text" name="nbReponses" placeholder="nb Réponses ">
    <br><br><br><br>
    <button class="btn waves-effect waves-light" type="submit" name="creer_question" value="Créer la question">Envoyer la question</button>
    </form>
    '; 
}
  else {
    echo '
    <form class="col s12" action="#" method="POST">'; 
    if ($nbReponses > 2){
    for ($i=1; $i<= $nbReponses; $i++){
      echo '<input type="text" name=reponse[]" >';
    }
  }
    echo '
    <br><br><br>
    <button class="btn waves-effect waves-light" type="submit" name="valider_reponses" value="Valider les réponses"> Valider les réponses</button>
    <input type="hidden"  name="question" value ="'.$_POST['question'].'">
    </form>
    ';

   
  }

?> 
  </div>

</body>
</html>