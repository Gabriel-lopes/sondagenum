<div class="row" align="center">

<?php 
foreach ($lesCategories as $uneCategorie) {
?>

    <div class="col l6 m6 s12">
    <div class="card">
    <div class="card-content">
      <span class="card-title activator grey-text text-darken-4"><?= $uneCategorie['nom']; ?></span>
    </div>
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="img/<?= $uneCategorie['nom']; ?>.jpg" height="300" width="100" alt="<?= $uneCategorie['description']; ?>">
    </div>
    <div class="card-content">
      <p><a href="index.php?idCategorie=<?= $uneCategorie['idCategorie']; ?>">Voir la catégorie</a></p>
    </div>
    <div class="card-reveal">
      	<span class="card-title grey-text text-darken-4"><?= $uneCategorie['nom']; ?><i class="material-icons right">close</i></span>
      	<p><?= $uneCategorie['description']; ?></p>
    </div>
  	</div>
    </div>

<?php 
}
?>

</div>