<?php 
$id = $_GET['resultat_sondage']; //On récupère l'id du sondage dans l'url
$idQ=$_GET['idQuestion'];
$total = 0; //Variable qui stock le total des votes

//Calcul du total des votes 
foreach($lesReponses as $uneReponse) { //On parcours la vue des votes pour récupérer quelques variables (B1)
	if($uneReponse['idSondage'] == $id and $idQ == $uneReponse['idQuestion']) { //Si l'id su sondage affiché est bien celui récupéré dans l'url (C1)
		//Affectations des varaibles pour faciliter leurs appels
		$total = $total + $uneReponse['nbReponse'];
		$question = $uneReponse['question'];
		$auteur = $uneReponse['nom'];
		$date = $uneReponse['dateCreation'];
	} //(C1)
} //(B1)
?>

<!-- Bloc qui permet d'afficher la date, la question et l'auteur du sondage -->

<p>Créé le <?= $date; ?></p>
<h5><?= $question; ?></h5>
<p>Par <?= $auteur; ?></p>
<br><br>

<?php


foreach($lesReponses as $uneReponse) { //On parcours la vue des votes pour récupérer quelques variables (B2)
	if($uneReponse['idSondage'] == $id and $idQ == $uneReponse['idQuestion'] ) { //Si l'id su sondage affiché est bien celui récupéré dans l'url (C2)
		//Variable qui va constituer le résultat en pourcent d'une réponse 
		if($uneReponse['nbReponse'] == 0) {
			$nombre = 0;
		} else {
			$nombre = $uneReponse['nbReponse']*100/$total;
		}
?>

<!-- reponse : pourcentage de votes pour cette réponse -->
<label><?= $uneReponse['reponse']; ?> : <?= round($nombre, 2); ?>%</label>
<br>
<!-- Barre de progression en relation avec le pourcentage -->
<progress value="<?= $uneReponse['nbReponse']; ?>" max="<?= $total; ?>"></progress>
<br><br>

<?php	
	} //(C2)
} //(B2)
?>