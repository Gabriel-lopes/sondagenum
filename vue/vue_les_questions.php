<?php
//if(isset($_SESSION['email'] AND $_SESSION['id'] == ))
?>

<table>
    <tr>
        <td>Questions</td> <td>Sondeur</td> <td>Date de création</td><td>Etat</td><td>Votants</td><td></td><td></td>
    </tr>

<?php
//On récupère l'id de la sous_categorie que l'on a choisi (dans l'url)
$id = $_GET['question_sondage'];

foreach($lesVuesSondages as $uneVueSondage) { //Boucle qui parcours la vue des sondages (B1)
  if($uneVueSondage['idSondage'] == $id) { //Si le sondage parcouru appartient bien à la sous catégorie alors il s'affiche (C1)
?>
        <!-- Tableau affichant les sondages -->
        <tr>
          <td><?= $uneVueSondage['question']; ?></td>
          <td><?= $uneVueSondage['nom']; ?></td>
          <td><?= $uneVueSondage['dateCreation']; ?></td>
          <td><?= $uneVueSondage['etat']; ?></td>
          <?php
          $nb = 0; 
          foreach ($lesVuesReponses as $uneVueReponse) {
            if($uneVueReponse['idQuestion'] == $uneVueSondage['idQuestion']) {
              $nb += $uneVueReponse['nbReponse'];
            }
          }
          ?>
          <td><?= $nb; ?></td>
          <td><a href="index.php?resultat_sondage=<?= $uneVueSondage['idSondage']; ?>&idQuestion=<?= $uneVueSondage['idQuestion']?>">Voir le résultat</td>
<?php
    //Condition si la session existe (Si connecté) (C2)
    if(isset($_SESSION['id'])) {
      //Condition si le sondeur est une personne et non une entreprise (C3)

        //Condition si le sondage est ouvert (etat) 
        if($uneVueSondage['etat'] == "Ouvert") { //(C4)
?>
          <td><a href="index.php?vote_sondage=<?= $uneVueSondage['idSondage']; ?>&idQuestion=<?= $uneVueSondage['idQuestion']?>">Voter</td>

        </tr>
<?php
        } //C4
       //C3
    } //C2
  } //C1
} //B1
?>

</table>