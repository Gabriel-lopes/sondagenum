<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>SondageNum.com - Connexion</title>
</head>
<body>
  <div id="left">

  <br>
  <br>
  <br>
  
    <blockquote>
      Bienvenue sur SondageNum,<br>
      Le site de sondage pour entreprises et particuliers,<br>
      créer par l'INSEE.
    </blockquote>
  </div>
<?php
if (!empty($_SESSION)) {

  echo '
  
  <div class="section"></div>
  
  <h5 class="indigo-text">Se connecter</h5>
  <div class="section"></div>
  
  <div class="container">
    <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
  
      <form class="col s12" method="post" action="">
        <div class="row">
          <div class="col s12">
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s12">
            <input class="validate" placeholder="Votre email" type="email" name="email"/>
            <label for="email">Votre Email</label>
          </div>
        </div>
  
        <div class="row">
          <div class="input-field col s12">
            <input class="validate" type="password" name="mdp" placeholder="Votre mot de passe"/>
            <label for="password">Mot de passe</label>
          </div>
        </div>
  
        <br />
        <center>
          <div class="row">
            <button type="submit" name="SeConnecter" value="SeConnecter" class="col s12 btn btn-large waves-effect indigo">Se connecter</button>
          </div>
        </center>
      </form>';
  }
  ?>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</body>
</html>