<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>SondageNum.com - Inscription</title>
    
</head>
<body>
<div class="row">
<h1 id="login">Inscription</h1><br>
    <form class="col s12" action=" " method="post">
        
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Nom" id="nom" type="text" name="nom"class="validate">
          <label for="nom">Nom</label>
        </div>

        <div class="input-field col s6">
          <input placeholder="Prénom" id="Prénom" type="text" name="prenom"class="validate">
          <label for="prenom">Prénom</label>
        </div>

        <div class="input-field col s6">
          <input placeholder="Téléphone" name="tel" type="text" class="validate" minlength="10" maxlength="10">
          <label for="tel">Téléphone</label>
        </div>

        <div class="input-field col s6">
          <input placeholder="Age" name="age" type="text" class="validate">
          <label for="age">Age</label>
       </div>

        <div class="input-field col s6">
          <input placeholder="Adresse" name="adresse" type="text" class="validate">
          <label for="adresse">Adresse</label>
       </div>
        
        <div class="input-field col s6">
          <input placeholder="Email" type="email" class="validate" name="email">
          <label for="email">Email</label>
        </div>

        <div class="input-field col s6">
          <input  placeholder="Mot de passe" type="password" class="validate" name="mdp">
          <label for="password">Password</label>
        </div>

        <br><br>
        <button class="btn waves-effect waves-light" type="submit" id="inscription_personne" name="inscription_personne" >S'inscrire
            <i class="material-icons right">send</i>
        </button>
    </form>
</div>

</div>

