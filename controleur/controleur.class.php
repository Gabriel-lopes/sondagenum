<?php

    require_once("modele/modele.class.php");

    class Controleur
    {
        
        private $unModele;

        public function __construct($server, $bdd ,$user, $mdp)
        {
            $this->unModele = new Modele($server, $bdd, $user, $mdp);
        }
        
        public function getTable()
        {
            return $this->unModele->getTable();
        }

        public function setTable($uneTable)
        {
            $this->unModele->setTable($uneTable);
        }

        public function selectAll()
        {
            return $this->unModele->selectAll();
        }

        public function insert($tab)
        {
            //on controle ici les données du fomrulaire.
            $this->unModele->insert($tab);
        }

        public function delete($where)
        {
            $this->unModele->delete($where);
        }

        public function selectWhere($where)
        {
            return $this->unModele->selectWhere($where); //c'est un return car la methode return un resultat de selectWhere (pour la method edit)
        }

        public function selectWhereAll($where)
        {
            return $this->unModele->selectWhereAll($where); //c'est un return car la methode return un resultat de selectWhere (pour la method edit)
        }

        public function update($tab, $where)
        {
            $this->unModele->update($tab, $where);
        }

        public function selectSearch($tab, $mot)
        {
            return $this->unModele->selectSearch($tab, $mot);
        }

        public function count()
        {
            return $this->unModele->count();
        }

        public function insertProcedure($procedure, $tab)
        {
            $this->unModele->insertProcedure($procedure, $tab);
        }

        public function updateVote($id) 
        {
            $this->unModele->updateVote($id);
        }
        public function findUserByEmail($email)
        {
            return $this->unModele->findUserByEmail($email);
        }
    }
?>